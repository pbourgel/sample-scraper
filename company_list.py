import re
import HTMLParser
HTMLParser.attrfind = re.compile(
    r'\s*([a-zA-Z_][-.:a-zA-Z_0-9]*)(\s*=\s*'
    r'(\'[^\']*\'|"[^"]*"|[^\s>^\[\]{}\|\'\"]*))?')
import requests
from BeautifulSoup import BeautifulSoup
from optparse import OptionParser
from decimal import *


usage = "usage: %prog [options]"

parser = OptionParser(usage)

parser.add_option("-e","--english",action="store_true",dest="english",help="Collect the list of companies on the English language version.")
parser.add_option("-g","--georgian",action="store_true",dest="georgian",help="Collect the list of companies on the Georgian language version.")
parser.add_option("-r","--russian",action="store_true",dest="russian",help="Collect the list of companies on the Russian language version.")

(options, args) = parser.parse_args()

if options.english:
	link = 'http://ema.gov.ge/content.php?id=94&lang=eng&s=1&form=&name=&idcode=&phone=&fax=&email=&status=&employee=&capital=&share=&juraddress=&address=&x=45&y=12'
	file_name = 'state_owned_companies_en.txt'
elif options.georgian:
	link = 'http://ema.gov.ge/content.php?id=94&lang=geo&s=1&form=&name=&idcode=&phone=&fax=&email=&status=&employee=&capital=&share=&juraddress=&address=&x=43&y=11'
	file_name = 'state_owned_companies_ge.txt'
elif options.russian:
	link = 'http://ema.gov.ge/content.php?id=94&lang=rus&s=1&form=&name=&idcode=&phone=&fax=&email=&status=&employee=&capital=&share=&juraddress=&address=&x=31&y=11'
	file_name = 'state_owned_companies_ru.txt'
else:
	print 'You must enter a language version.  Type python company_list.py followed by -e for English, -g for Georgian, or -r for Russian.'
	exit()

SITE_ROOT = 'http://ema.gov.ge/'
array_of_pages = []
comp_string = ''

#grab_company_share inspects a single page of companies and parses out its state share.
#If the state share is greater than or equal to 51%, we note its name and company ID and 
#append that information to a list.
def grab_company_share(page):
	#Find the link to the individual company pages, the eid GET argument is helpful here
	companies = []
	html = BeautifulSoup(page.content)
	x = html.findAll('a',href=re.compile('eid'))
	#For each company in the page of results
	for i in x:
		#Synthesize the link to the individual page
		company_page = SITE_ROOT + i['href']
		#Parse the HTML on this page		
		company_page_soup = BeautifulSoup(requests.get(company_page).content)
		#Grab the div tags that will include the percentage state share
		divs = company_page_soup.findAll('div',{'class' : 'productions-content-description'})
		#Grab the specific percentage (yes it's hackish, but I threw this together in a few hours)
		percentage = Decimal(divs[11].contents[1].contents[0][:-1])
		if percentage >= 50.01:  #If this company is majority state-owned
			#Find the div tag that includes the name information and parse it out
			div_name = company_page_soup.findAll('div', {'class' : 'productions-content-name'})[0]
			company_name = div_name.contents[0].contents[0]
			company_id = int(divs[0].contents[1].contents[0])
			#Put together the string we want: the name plus the id number
			comp_str = company_name + ' ' + str(company_id)
			print 'Saving data on company ' + company_name
			#Add this to the list
			companies.append(comp_str)
		else:
			print 'Ignoring company, ' +  'State share is ' + str(percentage)
	return companies

#write_results: Simple function to write a company name and its ID number to a file
def write_results(company_data):
	FILE = open(file_name,'a')
	for cd in company_data: 
		FILE.write(cd + '\n')
	FILE.close()


#Feed the first page into the parser
print 'Loading first page ' + str(link)
page = requests.get(link)
page_html = BeautifulSoup(page.content)

#Get the links to the lists of companies
page_links = page_html.findAll('div',{'class':'page-noactive'})
print 'parsed out links to lists of companies'

#Parse out the URLs for those lists
for page in page_links:
	page_url = SITE_ROOT + page.contents[0]['href']
	array_of_pages.append(page_url)

#Parse the first page of companies and figure out what is state owned
print 'Loading first list of companies'
companies = grab_company_share(requests.get(link))
write_results(companies)

#Use a for loop to click through all of the remaining lists and extract each company
for comp_page in array_of_pages:
	print 'Loading new list of companies'
	link = requests.get(comp_page)
	companies = grab_company_share(link)
	write_results(companies)

print 'Report complete'
